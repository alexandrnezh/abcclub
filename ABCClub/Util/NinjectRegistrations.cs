﻿using ABCClub.Domain.Interfaces;
using ABCClub.Infrastructure.Data;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCClub.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<ITariffRepository>().To<TariffRepository>();
            Bind<ICategoryRepository>().To<CategoryRepository>();
            Bind<IGroupRepository>().To<GroupRepository>();
            Bind<IArticleRepository>().To<ArticleRepository>();
            Bind<IBlockRepository>().To<BlockRepository>();
            Bind<ILevelRepository>().To<LevelRepository>();
        }
    }
}