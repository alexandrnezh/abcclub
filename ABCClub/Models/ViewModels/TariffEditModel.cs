﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCClub.Models.ViewModels
{
    public class TariffEditModel
    {
        public Tariff tariff;
        public List<Category> categories;
        public List<Group> groups;
    }
}