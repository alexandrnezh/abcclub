﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCClub.Models.ViewModels
{
    public class CategoriesGroupModel
    {
        public List<Category> categories;
        public List<Group> groups;
    }
}