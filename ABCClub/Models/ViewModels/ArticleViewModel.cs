﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCClub.Models.ViewModels
{
    public class ArticleViewModel
    {
        public List<Block> blocks { get; set; }
        public Article article { get; set; }
    }
}