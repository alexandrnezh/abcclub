﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ABCClub.Domain.Core
{
    public class Tariff
    {
        public int Id { get; set; }
        public int LessonNum { get; set; }
        public double Price { get; set; }
        public bool ChildLessonTime { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        [ForeignKey("GroupId")]
        public Group Group { get; set; }
        [Required]
        public int GroupId { get; set; }
        [Required]
        public int CategoryId { get; set; }

    }
}
