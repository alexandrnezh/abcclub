﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ABCClub.Domain.Core
{
    public class Block
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsImage { get; set; }
        [ForeignKey("ArticleId")]
        public Article Article { get; set; }
        [Required]
        public int ArticleId { get; set; }
    }
}
