﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCClub.Domain.Core
{
    public class Level
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FirstParagraphTitle { get; set; }
        public string FirstParagraph { get; set; }
        public string SecondParagraphTitle { get; set; }
        public string SecondParagraph { get; set; }
        public string FirstImage { get; set; }
        public string SecondImage { get; set; }

    }
}