﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABCClub.Domain.Core
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
