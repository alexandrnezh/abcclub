﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;

namespace ABCClub.Domain.Interfaces
{
    public interface ITariffRepository : IDisposable
    {
        List<Tariff> GetTarrifs();
        List<Tariff> GetTariffsByCategoryName(string name,string group);
        List<Tariff> GetTariffsByCategoryGroupName(string category,string group);
        Tariff GetTariff(int id);
        void Create(Tariff item);
        void Update(Tariff item);
        void Delete(int id);
        void Save();
    }
}
