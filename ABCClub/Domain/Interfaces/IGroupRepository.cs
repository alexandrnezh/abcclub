﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABCClub.Domain.Interfaces
{
    public interface IGroupRepository : IDisposable
    {
        List<Group> GetGroups();
        Group GetGroup(int id);
        void Create(Group item);
        void Update(Group item);
        void Delete(int id);
        void Save();
    }
}
