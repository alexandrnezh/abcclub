﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABCClub.Domain.Interfaces
{
    public interface IBlockRepository : IDisposable
    {
        List<Block> GetBlocks();
        List<Block> GetBlocksByArticleId(int id);
        Block GetBlock(int id);
        void Create(Block item);
        void Update(Block item);
        void Delete(int id);
        void Save();
    }
}
