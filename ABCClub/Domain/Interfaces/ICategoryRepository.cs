﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;

namespace ABCClub.Domain.Interfaces
{
    public interface ICategoryRepository : IDisposable
    {
        List<Category> GetCategories();
        Category GetCategory(int id);
        void Create(Category item);
        void Update(Category item);
        void Delete(int id);
        void Save();
    }
}
