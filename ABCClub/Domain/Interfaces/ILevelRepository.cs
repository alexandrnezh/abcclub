﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABCClub.Domain.Interfaces
{
    public interface ILevelRepository : IDisposable
    {
        List<Level> GetLevels();
        Level GetLevel(int id);
        void Create(Level item);
        void Update(Level item);
        void Delete(int id);
        void Save();
    }
}
