﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;

namespace ABCClub.Domain.Interfaces
{
    public interface IArticleRepository : IDisposable
    {
        List<Article> GetArticles();
        Article GetArticle(int id);
        void Create(Article item);
        void Update(Article item);
        void Delete(int id);
        void Save();
    }
}
