﻿$(document).ready(function () {
    $(document).delegate('.open', 'click', function (event) {
        if ($('.open').hasClass('oppenned')) {
            $(this).removeClass('oppenned');
        } else {
            $(this).addClass('oppenned');
        }
        $('.header_logo2').toggle();
        event.stopPropagation();
    })

    $(document).delegate('body', 'click', function (event) {
        if ($('.open').hasClass('oppenned')) {
            $('.header_logo2').toggle();
        }
        $('.open').removeClass('oppenned');
    })


});

