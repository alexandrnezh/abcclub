﻿using ABCClub.Domain.Core;
using ABCClub.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace ABCClub.Infrastructure.Data
{
    public class GroupRepository : IGroupRepository
    {
        private TariffContext db;
        public GroupRepository()
        {
            this.db = new TariffContext();
        }
        public void Create(Group item)
        {
            db.Groups.Add(item);
        }

        public void Delete(int id)
        {
            Group group = db.Groups.Find(id);
            if (group != null)
            {
                db.Groups.Remove(group);
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<Group> GetGroups()
        {
            return db.Groups.ToList();
        }

        public Group GetGroup(int id)
        {
            return db.Groups.Find(id);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Group item)
        {
            db.Entry(item).State = EntityState.Detached;
            //db.Entry(item).State = EntityState.Modified;
        }
    }
}
