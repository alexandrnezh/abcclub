﻿using ABCClub.Domain.Core;
using ABCClub.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ABCClub.Infrastructure.Data
{
    public class LevelRepository : ILevelRepository
    {
        private TariffContext db;
        public LevelRepository()
        {
            this.db = new TariffContext();
        }
        public void Create(Level item)
        {
            db.Levels.Add(item);
        }

        public void Delete(int id)
        {
            Level level = db.Levels.Find(id);
            if (level != null)
            {
                db.Levels.Remove(level);
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Level GetLevel(int id)
        {
            return db.Levels.Find(id);
        }

        public List<Level> GetLevels()
        {
            return db.Levels.ToList();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Level item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}