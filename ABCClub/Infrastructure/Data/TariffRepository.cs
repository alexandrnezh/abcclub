﻿using ABCClub.Domain.Core;
using ABCClub.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace ABCClub.Infrastructure.Data
{
    public class TariffRepository : ITariffRepository
    {
        private TariffContext db;
        public TariffRepository()
        {
            this.db = new TariffContext();
        }

        public void Create(Tariff item)
        {
            db.Tariffs.Add(item);
        }

        public void Delete(int id)
        {
            Tariff tariff = db.Tariffs.Find(id);
            if (tariff!=null)
            {
                db.Tariffs.Remove(tariff);
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Tariff GetTariff(int id)
        {
            return db.Tariffs.Find(id);
        }

        public List<Tariff> GetTarrifs()
        {
            return db.Tariffs.ToList();
        }
        public List<Tariff> GetTariffsByCategoryName(string name,string group)
        {
            return db.Tariffs.Where(t => t.Category.Name == name && t.Group.Name==group).ToList();
        }
        public List<Tariff> GetTariffsByCategoryGroupName(string category,string group)
        {
            return db.Tariffs.Where(t => t.Category.Name == category && t.Group.Name==group).ToList();
        }
        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Tariff item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
