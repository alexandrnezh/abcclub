﻿using ABCClub.Domain.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace ABCClub.Infrastructure.Data
{
    public class TariffContext : DbContext
    {
        public TariffContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Block> Blocks { get; set; }
        public DbSet<Level> Levels { get; set; }
    }

    public class DatabaseInitializer : CreateDatabaseIfNotExists<TariffContext>
    {
        protected override void Seed(TariffContext ctx)
        {
            Category category1 = new Category { Name = "Для детей" };
            Category category2 = new Category { Name = "Для подростков" };
            Category category3 = new Category { Name = "Для взрослых" };

            Group group1 = new Group { Name = "Группа"};
            Group group2 = new Group { Name = "Индивидуально" };

            Tariff tariff1 = new Tariff { Category = category1,CategoryId=category1.Id, Price = 2000, LessonNum = 8, ChildLessonTime = true ,Group=group1,GroupId=group1.Id};
            Tariff tariff2 = new Tariff { Category = category1, CategoryId = category1.Id, Price = 1600, LessonNum = 16, ChildLessonTime = true , Group = group2, GroupId = group2.Id };
            Tariff tariff3 = new Tariff { Category = category1, CategoryId = category1.Id, Price = 1200, LessonNum = 24, ChildLessonTime = true, Group = group1, GroupId = group1.Id };
            Tariff tariff4 = new Tariff { Category = category2, CategoryId = category2.Id, Price = 3000, LessonNum = 8, ChildLessonTime = false, Group = group1, GroupId = group1.Id };
            Tariff tariff5 = new Tariff { Category = category2, CategoryId = category2.Id, Price = 4000, LessonNum = 16, ChildLessonTime = false, Group = group2, GroupId = group2.Id };
            Tariff tariff6 = new Tariff { Category = category2, CategoryId = category2.Id, Price = 5000, LessonNum = 24, ChildLessonTime = false, Group = group1, GroupId = group1.Id };
            Tariff tariff7 = new Tariff { Category = category3, CategoryId = category3.Id, Price = 6000, LessonNum = 8, ChildLessonTime = false, Group = group1, GroupId = group1.Id };
            Tariff tariff8 = new Tariff { Category = category3, CategoryId = category3.Id, Price = 7000, LessonNum = 16, ChildLessonTime = false, Group = group2,GroupId=group2.Id };
            Tariff tariff9 = new Tariff { Category = category3, CategoryId = category3.Id, Price = 8000, LessonNum = 24, ChildLessonTime = false, Group = group1, GroupId = group1.Id };

            ctx.Tariffs.AddRange(new List<Tariff> { tariff1,tariff2,tariff3,tariff4,tariff5,tariff6,tariff7,tariff8,tariff9});
            ctx.SaveChanges();
        }
    }
}
