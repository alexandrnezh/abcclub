﻿using ABCClub.Domain.Core;
using ABCClub.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace ABCClub.Infrastructure.Data
{
    public class ArticleRepository : IArticleRepository
    {
        private TariffContext db;
        public ArticleRepository()
        {
            this.db = new TariffContext();
        }
        public void Create(Article item)
        {
            db.Articles.Add(item);
        }

        public void Delete(int id)
        {
            Article article = db.Articles.Find(id);
            if (article != null)
            {
                db.Articles.Remove(article);
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<Article> GetArticles()
        {
            return db.Articles.ToList();
        }

        public Article GetArticle(int id)
        {
            return db.Articles.Find(id);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Article item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
