﻿using ABCClub.Domain.Core;
using ABCClub.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace ABCClub.Infrastructure.Data
{
    public class BlockRepository : IBlockRepository
    {
        private TariffContext db;
        public BlockRepository()
        {
            this.db = new TariffContext();
        }
        public void Create(Block item)
        {
            db.Blocks.Add(item);
        }

        public void Delete(int id)
        {
            Block block = db.Blocks.Find(id);
            if (block != null)
            {
                db.Blocks.Remove(block);
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<Block> GetBlocks()
        {
            return db.Blocks.ToList();
        }

        public List<Block> GetBlocksByArticleId(int id)
        {
            return db.Blocks.Where(b => b.ArticleId== id).ToList();
        }

        public Block GetBlock(int id)
        {
            return db.Blocks.Find(id);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Update(Block item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
