﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ABCClub.Startup))]
namespace ABCClub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
