﻿using ABCClub.Domain.Core;
using ABCClub.Domain.Interfaces;
using ABCClub.Infrastructure.Data;
using ABCClub.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace ABCClub.Controllers
{
    public class HomeController : Controller
    {
        public static string category = "Для детей";
        public static string group = "Группа";
        ITariffRepository tariffRepository;
        ICategoryRepository categoryRepository;
        IGroupRepository groupRepository;
        IArticleRepository articleRepository;
        IBlockRepository blockRepository;
        ILevelRepository levelRepository;
        TariffContext context;

        public HomeController(ITariffRepository tariffRepository, ICategoryRepository categoryRepository, IGroupRepository groupRepository, IArticleRepository articleRepository, IBlockRepository blockRepository, ILevelRepository levelRepository, TariffContext context)
        {
            this.tariffRepository = tariffRepository;
            this.categoryRepository = categoryRepository;
            this.groupRepository = groupRepository;
            this.articleRepository = articleRepository;
            this.blockRepository = blockRepository;
            this.levelRepository = levelRepository;
            this.context = context;
        }

        public ActionResult LevelAction()
        {
            var levels=levelRepository.GetLevels();
            return PartialView("_ViewLevels",levels);
        }
        public ActionResult Index()
        {
            var tariffs = tariffRepository.GetTarrifs();
            var categories = categoryRepository.GetCategories();
            var groups = groupRepository.GetGroups();
            var levels = levelRepository.GetLevels();
            return View(new TariffViewModel { tariffs = tariffs, categories = categories, groups = groups, levels=levels });
        }

        public ActionResult Contacts()
        {
            return View();
        }
        public ActionResult ContentLoad()
        {
            return View();
        }


        public ActionResult Tariffs()
        {
            var tariffs = tariffRepository.GetTarrifs();
            var categories = categoryRepository.GetCategories();
            var groups = groupRepository.GetGroups();
            return View(new TariffViewModel { tariffs = tariffs, categories = categories, groups = groups });
        }

        public ActionResult Articles()
        {
            var articles = articleRepository.GetArticles();
            articles.Reverse();
            return View(articles);
        }

        public ActionResult Article(int id)
        {
            var blocks = blockRepository.GetBlocksByArticleId(id);
            var article = articleRepository.GetArticle(id);
            return View(new ArticleViewModel() { blocks = blocks, article = article });
        }

        public ActionResult Level(int id)
        {
            var level = levelRepository.GetLevel(id);
            return View(level);
        }

        public ActionResult SearchByCategory(string name)
        {
            var tariffs = tariffRepository.GetTariffsByCategoryName(name, group);
            category = name;
            return PartialView("_ViewTariffs", tariffs);
        }

        public ActionResult SearchByGroup(string name)
        {
            group = name;
            var tariffs = tariffRepository.GetTariffsByCategoryGroupName(category, name);
            return PartialView("_ViewTariffs", tariffs);
        }

        public ActionResult SearchByChildrenTime(string name)
        {
            group = name;
            var tariffs = tariffRepository.GetTariffsByCategoryGroupName(category, name);
            return PartialView("_ViewTariffs", tariffs);
        }


        //Admin
        public ActionResult AddTariff()
        {
            var categories = categoryRepository.GetCategories();
            var groups = groupRepository.GetGroups();
            return View(new CategoriesGroupModel() { categories = categories, groups = groups });
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddTariff(Tariff tariff)
        {
            if (tariff != null)
            {
                tariffRepository.Create(new Tariff
                {
                    LessonNum = tariff.LessonNum,
                    Price = tariff.Price,
                    ChildLessonTime = tariff.ChildLessonTime,
                    CategoryId = tariff.CategoryId,
                    GroupId = tariff.GroupId
                });
                tariffRepository.Save();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult AddArticle()
        {
            return View();
        }

        public ActionResult AddLevel()
        {
            return View();
        }

        public ActionResult DeleteLevel(int id)
        {
            if (id != null)
            {
                levelRepository.Delete(id);
                levelRepository.Save();
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddLevel(Level level, HttpPostedFileBase firstImage, HttpPostedFileBase secondImage)
        {
            if (level != null)
            {
                string firstImageName = System.IO.Path.GetFileName(firstImage.FileName);
                string secondImageName = System.IO.Path.GetFileName(secondImage.FileName);
                levelRepository.Create(new Level() { Title = level.Title, FirstImage = firstImageName, SecondImage = secondImageName, FirstParagraph = level.FirstParagraph, SecondParagraph = level.SecondParagraph, FirstParagraphTitle = level.FirstParagraphTitle, SecondParagraphTitle = level.SecondParagraphTitle });
                levelRepository.Save();

                //Загрузка картинок на сервер
                if (firstImage != null)
                {
                    SaveFile(firstImage);
                }
                if (secondImage != null)
                {
                    SaveFile(secondImage);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        [Authorize]
        [HttpPost]
        public ActionResult AddArticle(Article article, IEnumerable<HttpPostedFileBase> uploads, IEnumerable<string> array, HttpPostedFileBase image)
        {
            if (article != null)
            {
                string imageName = System.IO.Path.GetFileName(image.FileName);
                articleRepository.Create(new Article
                { Date = DateTime.Now, Description = article.Description, Title = article.Title, Image = imageName });
                articleRepository.Save();

                var articles = articleRepository.GetArticles();
                int articleId = articles.Last().Id;
                int count = 0;

                //Добавление массива блоков статьи
                if (array != null)
                {
                    foreach (var item in array)
                    {
                        if (item == "")
                        {
                            //Добавление блока картинки
                            string filename = System.IO.Path.GetFileName(uploads.ElementAt(count).FileName);
                            blockRepository.Create(new Block() { IsImage = true, Text = filename, ArticleId = articleId });
                            count++;
                        }
                        else
                        {
                            //Добавление блока абзаца
                            blockRepository.Create(new Block() { IsImage = false, Text = item, ArticleId = articleId });
                        }
                        blockRepository.Save();
                    }
                    //Загрузка картинок на сервер
                    if (uploads != null)
                    {
                        foreach (var file in uploads)
                        {
                            if (file != null)
                            {
                                SaveFile(image);
                            }

                        }
                    }
                    if (image != null)
                    {
                        SaveFile(image);
                    }

                }
                return RedirectToAction("Articles");
            }
            else
            {
                return RedirectToAction("Articles");
            }

        }

        public ActionResult EditTariff(int id)
        {
            if (id != null)
            {
                var tariff = tariffRepository.GetTariff(id);
                var categories = categoryRepository.GetCategories();
                var groups = groupRepository.GetGroups();
                return View(new TariffEditModel() { categories = categories, groups = groups, tariff = tariff });
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        [Authorize]
        [HttpPost]
        public ActionResult EditTariff(Tariff tariff)
        {
            if (tariff != null)
            {
                var Tariff = tariffRepository.GetTariff(tariff.Id);
                Tariff.Price = tariff.Price;
                Tariff.LessonNum = tariff.LessonNum;
                Tariff.Group = null;
                Tariff.GroupId = tariff.Group.Id;
                tariffRepository.Save();
                var groups = groupRepository.GetGroups();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditArticle(int id)
        {
            if (id != null)
            {
                var blocks = blockRepository.GetBlocksByArticleId(id);
                var article = articleRepository.GetArticle(id);
                return View(new ArticleViewModel() { blocks = blocks, article = article });
            }
            else
            {
                return RedirectToAction("Articles");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditArticle(Article article, IEnumerable<HttpPostedFileBase> uploads, IEnumerable<string> array, IEnumerable<string> images, HttpPostedFileBase image)
        {
            if (article != null)
            {

                var Article = articleRepository.GetArticle(article.Id);
                Article.Title = article.Title;
                Article.Description = article.Description;
                if (image != null)
                {
                    string imageName = System.IO.Path.GetFileName(image.FileName);
                    Article.Image = imageName;
                }

                articleRepository.Save();

                //Удаление старого массива блоков статьи
                var oldBlocks = blockRepository.GetBlocksByArticleId(Article.Id);
                foreach (var item in oldBlocks)
                {
                    blockRepository.Delete(item.Id);
                    blockRepository.Save();
                }

                //Добавление массива блоков статьи
                int count = 0;
                foreach (var item in array)
                {
                    if (item == "")
                    {
                        var arrayImage = images.ElementAt(count);
                        if (arrayImage == "")
                        {
                            //Добавление блока картинки
                            string filename = System.IO.Path.GetFileName(uploads.ElementAt(count).FileName);
                            blockRepository.Create(new Block() { IsImage = true, Text = filename, ArticleId = Article.Id });

                        }
                        else
                        {
                            //Добавление блока абзаца
                            blockRepository.Create(new Block() { IsImage = true, Text = arrayImage, ArticleId = Article.Id });
                        }
                        count++;
                    }
                    else
                    {
                        blockRepository.Create(new Block() { IsImage = false, Text = item, ArticleId = Article.Id });
                    }
                    blockRepository.Save();
                }

                //Загрузка картинок на сервер
                if (uploads != null)
                {
                    foreach (var file in uploads)
                    {
                        if (file != null)
                        {
                            SaveFile(file);
                        }
                    }
                }
                if (image != null)
                {
                    SaveFile(image);
                }

                return RedirectToAction("Articles");
            }
            else
            {
                return RedirectToAction("Articles");
            }

        }

        public ActionResult DeleteTariff(int id)
        {
            if (id != null)
            {
                tariffRepository.Delete(id);
                tariffRepository.Save();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeleteArticle(int id)
        {
            if (id != null)
            {
                articleRepository.Delete(id);
                articleRepository.Save();
            }
            return RedirectToAction("Articles");
        }


        //Functions
        protected override void Dispose(bool disposing)
        {
            tariffRepository.Dispose();
            categoryRepository.Dispose();
            base.Dispose(disposing);
        }

        public void SaveFile(HttpPostedFileBase file)
        {
            string fileName = Path.GetFileName(file.FileName);
            string filePath = Path.Combine(Server.MapPath("/Images"), Path.GetFileName(fileName));
            file.SaveAs(filePath);
        }
    }


}